<?php
/*
* Template name: BMW-Landingpage-rational
* Template Post Type: page
*/

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" style="margin-left: 5%; margin-right: 5%">
    <div class="site-title">
        <div class="site-title-bg">
            <h1><a href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
            <?php
            $description = get_bloginfo('description', 'display');
            if ($description || is_customize_preview()) : ?>
                <p class="site-description"><?php echo esc_html($description); ?></p>
            <?php endif; ?>
        </div>
    </div>
    <?php blank_custom_logo(); ?>
    <h2>Willkommen.</h2>
    <h3>Entdecken Sie die welt von BMW.<br>
        Gleich in Ihrer Nähe.</h3>
    <p>
        Wir haben den ganzen Tag mit Autos zu tun. Und das ist auch gut so! Weil es uns richtig viel Spaß macht. Wir
        gehen mit Elan ans Werk – und haben bei jedem einzelnen Kunden erneut das Ziel vor Augen, ihn
        zufriedenzustellen. Unser großes Team aus Verkaufsberatern und Servicemitarbeitern kennt sich bestens mit großen
        und kleinen Fahrzeugen aus – und stellt Ihnen seine Kompetenz zur Verfügung. Dabei handeln wir stets nach
        unserem Credo: Wir tun mehr. Und Sie werden es spüren!
    </p>
    <div><p>pagelist:</p>
        <ul>
            <?php
            $pages = get_pages();
            foreach ($pages as $page) {
                echo '<li>' . $page->post_title . '</li>';
            } ?>
        </ul>
    </div>

    <!-- Insert landingpage content -->
    <?php if (shortcode_exists('fs_initRational')) {
        $contentBlock = do_shortcode("[fs_initRational targetid='targetForm']");
        echo '<div>' . $contentBlock . '</div>';
    } ?>

    <form name="rfo" id="rfo" class="form" action="http://bmw-musterhaus1-ort1.web01.adwerba.at/wp-admin/admin-post.php"
          method="post"><input type="hidden" name="action" value="bmw_form"><input type="hidden" name="brand"
                                                                                   value="bmw"><input type="hidden"
                                                                                                      name="page_form"
                                                                                                      value="/faststart-playful/"><input
                type="hidden" name="Response_BGVProductConfiguration_Brand" value="BMW">
        <div class="row">
            <div class="col col-xs-12 col-md-6">
                <div class="form-group "><label class="control-label"
                                                for="Response_BGVProductConfiguration_ProductId_BodyType">Fahrzeug
                        *</label><select class="form-control"
                                         name="Response_BGVProductConfiguration_ProductId_BodyType">
                        <option value="">Fahrzeug *</option>
                        <option value="F21-HC">
                            BMW 1er 3-Türer
                        </option>
                        <option value="F20-SH">
                            BMW 1er 5-Türer
                        </option>
                        <option value="F22-CP">
                            BMW 2er Coupé
                        </option>
                        <option value="F23-CA">
                            BMW 2er Cabrio
                        </option>
                        <option value="F45-ST">
                            BMW 2er Active Tourer
                        </option>
                        <option value="2C71">
                            BMW 2er Active Tourer iPerformance
                        </option>
                        <option value="F46-MP">
                            BMW 2er Gran Tourer
                        </option>
                        <option value="F31-TO">
                            BMW 3er Touring
                        </option>
                        <option value="F34-ST">
                            BMW 3er Gran Turismo
                        </option>
                        <option value="F32-CP">
                            BMW 4er Coupé
                        </option>
                        <option value="F33-CA">
                            BMW 4er Cabrio
                        </option>
                        <option value="F36-CO">
                            BMW 4er Gran Coupé
                        </option>
                        <option value="G30-LI">
                            BMW 5er Limousine
                        </option>
                        <option value="JA91">
                            BMW 5er Limousine iPerformance
                        </option>
                        <option value="G31-TO">
                            BMW 5er Touring
                        </option>
                        <option value="G32-ST">
                            BMW 6er Gran Turismo
                        </option>
                        <option value="G11-LI">
                            BMW 7er Limousine
                        </option>
                        <option value="7D01">
                            BMW 7er Limousine iPerformance
                        </option>
                        <option value="G12-LI">
                            BMW 7er Limousine Langversion
                        </option>
                        <option value="G15-CP">
                            BMW 8er Coupé
                        </option>
                        <option value="F48-GF">
                            BMW X1
                        </option>
                        <option value="F39-SC">
                            BMW X2
                        </option>
                        <option value="G01-GF">
                            BMW X3
                        </option>
                        <option value="G02-SC">
                            BMW X4
                        </option>
                        <option value="G05-GF">
                            BMW X5
                        </option>
                        <option value="F16-SC">
                            BMW X6
                        </option>
                        <option value="F87-CP">
                            BMW M2 Competition
                        </option>
                        <option value="F82-CP">
                            BMW M4 Coupé
                        </option>
                        <option value="F83-CA">
                            BMW M4 Cabrio
                        </option>
                        <option value="F90-LI">
                            BMW M5 Limousine
                        </option>
                        <option value="I01-HB">
                            BMW i3
                        </option>
                        <option value="I12-CP">
                            BMW i8 Coupé
                        </option>
                        <option value="I15-RO">
                            BMW i8 Roadster
                        </option>
                        <option value="I01-HB">
                            BMW i3
                        </option>
                        <option value="I12-CP">
                            BMW i8 Coupé
                        </option>
                        <option value="I15-RO">
                            BMW i8 Roadster
                        </option>
                    </select></div>
            </div>
        </div>


        <div class="row">
            <div class="col col-xs-12 col-sm-6">
                <div class="form-group "><label class="control-label" for="Customer_Salutation">Anrede *</label><select
                            class="form-control" name="Customer_Salutation">
                        <option value="">Anrede *</option>
                        <option value="MISS">
                            Frau
                        </option>
                        <option value="MR">
                            Herr
                        </option>
                    </select></div>
            </div>
            <div class="col col-xs-12 col-sm-6">
                <div class="form-group "><label class="control-label" for="Customer_AcademicTitle">Titel</label><select
                            class="form-control" name="Customer_AcademicTitle">
                        <option value="">Titel</option>
                        <option value="ACADEMIC_TITLE_CARMEN_114">
                            Dipl.Ing.
                        </option>
                        <option value="ACADEMIC_TITLE_CARMEN_195">
                            Dr.
                        </option>
                        <option value="ACADEMIC_TITLE_CARMEN_294">
                            Ing.
                        </option>
                        <option value="ACADEMIC_TITLE_CARMEN_705">
                            MA
                        </option>
                        <option value="ACADEMIC_TITLE_CARMEN_382">
                            Mag.
                        </option>
                        <option value="ACADEMIC_TITLE_CARMEN_708">
                            MBA
                        </option>
                    </select></div>
            </div>

        </div>

        <div class="row">
            <div class="col col-xs-12 col-sm-6">

                <div class="form-group "><label class="control-label" for="Customer_FirstName">Vorname *</label><input
                            type="text" class="form-control text" name="Customer_FirstName" id="Customer_FirstName"
                            placeholder="Vorname *" value=""></div>
            </div>
            <div class="col col-xs-12 col-sm-6">

                <div class="form-group "><label class="control-label" for="Customer_LastName">Nachname *</label><input
                            type="text" class="form-control text" name="Customer_LastName" id="Customer_LastName"
                            placeholder="Nachname *" value=""></div>
            </div>
        </div>

        <div class="row">
            <div class="col col-xs-12 col-sm-6">

                <div class="form-group "><label class="control-label" for="Customer_Email">E-Mail-Adresse
                        *</label><input type="text" class="form-control text" name="Customer_Email" id="Customer_Email"
                                        placeholder="E-Mail-Adresse *" value=""></div>
            </div>
            <div class="col col-xs-12 col-sm-6">
                <div class="form-group "><label class="control-label" for="Customer_Phone">Telefonnummer *</label><input
                            type="text" class="form-control text" name="Customer_Phone" id="Customer_Phone"
                            placeholder="Telefonnummer *" value=""></div>
            </div>
        </div>
    </form>

        <footer class="site-footer">
            <div class="site-info">
                <?php esc_html_e('BMW HO Mockup'); ?>
                <?php /* translators: Proudly powered by WordPress */ ?>
                - <a href="<?php echo esc_url(__('https://wordpress.org/', 'BMW-Testsite')); ?>">
                    <?php printf(esc_html__('Proudly powered by %s', 'BMW-Testsite'), 'WordPress'); ?>
                </a>
            </div>
        </footer>
</div><!-- #page -->
<?php wp_footer(); ?></body>
</html>
