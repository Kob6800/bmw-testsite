<?php
/**
 * Intentionally Blank Theme functions
 *
 * @package WordPress
 * @subpackage BMW-Testsite
 */

if ( ! function_exists( 'blank_setup' ) ) :
	/**
	 * Sets up theme defaults and registers the various WordPress features that
	 * this theme supports.
	 */
	function blank_setup() {
		load_theme_textdomain( 'BMW-Testsite' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );

		// This theme allows users to set a custom background.
		add_theme_support( 'custom-background', apply_filters( 'intentionally_blank_custom_background_args', array(
			'default-color' => 'f5f5f5',
		) ) );

		add_theme_support( 'custom-logo' );
		add_theme_support( 'custom-logo', array(
			'height'      => 256,
			'width'       => 256,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		/**
		 * Sets up theme defaults and registers the various WordPress features that
		 * this theme supports.
		 */
		function blank_custom_logo() {
			if ( function_exists( 'the_custom_logo' ) ) {
				the_custom_logo();
			}
		}
	}
endif; // end function_exists blank_setup.
add_action( 'after_setup_theme', 'blank_setup' );

add_action( 'customize_register', function( $wp_customize ) {
    $wp_customize->remove_section( 'static_front_page' );
} );



//------------------------------- Customize
function bmwtestsite_enqueue_addwerba_style() {
    wp_enqueue_style( 'bmwtestsite-addwerba-style', get_template_directory_uri() . '/css/style.css' );
}

add_action('wp_enqueue_scripts','bmwtestsite_enqueue_addwerba_style');

function debug_to_console( $data ) {
    if ( is_array( $data ) )
        $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
    else
        $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
    echo $output;
}